# Mailbox Notifier

This is a ESP32 project using Rust to report when a mailbox is opened. This
project principally interacts with Home Assistant through a MQTT broker.

![Liligo T5_V2.3 epaper demo board](docs/demo-board.png)

This project is based on the [Liligo T5_V2.3 epaper demo board](https://www.aliexpress.com/item/32869729970.html)
and uses just a couple of switches to detect the mailbox door being opened.
Since there is an epaper display, it is possible to provide feed back to the
postal carrier. This was done primarily for picking up a package that does
not fit in the mailbox. The epaper display can inform the postal carrier if
someone is available or not to receive the package.

The display could also be used to display messages for the postal carrier
such as seasonal messages and wishing them well.

## Building an image


## Schematic


## Home Assistant interface

